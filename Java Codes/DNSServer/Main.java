/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author malikali
 */
public class Main {

    /**
     * @param args the command line arguments
     */


    private static int myPort ;
    private static String myIP;
    private static String inputFolder = "input";
    private static String configFile = "myconfig";
    private static String entryFiles = "entries";
    private static HashMap<String , String> DNSTable = new HashMap<String, String>();
    public static void main(String[] args) throws SocketException {
        try {


            configure();
            initializeTable();
            DatagramSocket mSocket = new DatagramSocket(myPort, InetAddress.getByName(myIP));

            System.out.println("Listening on port :"+myPort);
            while(true)
            {
                byte[] reqPacket = new byte[1024];
                DatagramPacket dp = new DatagramPacket(reqPacket , reqPacket.length);
                mSocket.receive(dp);
                String website = new String( dp.getData());
                website = website.trim();
                String ip = lookup(website);
                if(ip == null)
                {
                    ip = "notFound";
                }
                byte[] ipBytes = ip.getBytes();
                DatagramPacket sendingPacket = new DatagramPacket(ipBytes , ipBytes.length , dp.getAddress() , dp.getPort());
                mSocket.send(sendingPacket);
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private static void configure()
    {
        try {

            File fil = new File(inputFolder + File.separatorChar + configFile);
            InputStreamReader is;
            is = new FileReader(fil);
            BufferedReader bf = new BufferedReader(is);
            String s;
            s = bf.readLine();
            String[] split = s.split(" ");
            myIP = split[0];
            myPort = Integer.parseInt(split[1]);

            bf.close();

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void initializeTable()
    {
        try {
            File fil = new File(inputFolder + File.separatorChar + entryFiles);
            InputStreamReader is;
            is = new FileReader(fil);
            BufferedReader bf = new BufferedReader(is);
            String s;
            while ((s = bf.readLine()) != null)
            {
                 String[] split = s.split(" ");
                 DNSTable.put(split[0], split[1]);


            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
    private static String lookup(String Website)
    {
        Website =  Website.replace('.', ' ');
        String []splits = Website.split(" ");

        if(splits.length != 0)
        {
            if(splits[0].equals("www"))
            {
                Website = "";
                for (int i = 1 ; i < splits.length ; i++)
                    Website = splits[i]+".";
            }
            else
               Website =  Website.replace(' ', '.');

        }
        if(DNSTable.containsKey(Website))
        {
            return DNSTable.get(Website);
        }
        return null;
        
    }
}

