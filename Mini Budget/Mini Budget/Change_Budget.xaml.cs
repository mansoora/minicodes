﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;
namespace Mini_Budget
{
    public partial class Change_Budget : PhoneApplicationPage
    {
        public Change_Budget()
        {
            InitializeComponent();
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            if (txt_Amount.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Enter Amount", "Error", MessageBoxButton.OK);
                return;
            }
            try
            {
                int.Parse(txt_Amount.Text.Trim());
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {

                    using (StreamWriter stream = new StreamWriter(store.OpenFile(MainPage.buget, FileMode.OpenOrCreate, FileAccess.Write)))
                    {
                        
                        stream.WriteLine(txt_Amount.Text);
                        stream.Close();
                    }

                    MessageBox.Show("Sucessfull");
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Enter Amount in Numbers", "Error", MessageBoxButton.OK);
                return;

            }
        }
    }
}