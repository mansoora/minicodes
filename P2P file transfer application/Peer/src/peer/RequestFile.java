/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package peer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import objectsender.Request;

/**
 *
 * @author malikali
 */


    public class RequestFile extends Thread{

    Request res;
    String receivedFile = "receivedfiles";
    public RequestFile(Request r)
    {
        res = r;
        File rec = new File(receivedFile);
        if(!rec.exists())
        {
            rec.mkdir();
        }
    }

    private void transferData(InputStream in, OutputStream out)
			throws IOException
	{
		byte b[] = new byte[FT.BLOCK_SIZE];
		int amount;

		while ((amount = in.read(b)) > 0)
		{
			out.write(b, 0, amount);
		}
	}


    @Override
    public void run()
    {
        try {
               Socket s = new Socket(res.IP, res.portNUMBER);
               //ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
               //oos.writeObject(new String("Hello world"));
               InputStream is = s.getInputStream();

               File outputfile = new File(receivedFile+File.separatorChar+res.fileName);
               FileOutputStream fos = new FileOutputStream(outputfile);
               transferData(is, fos);
               Main.print("File Received");
               fos.close();
               is.close();

        } catch (UnknownHostException ex) {
            Logger.getLogger(RequestFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RequestFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
