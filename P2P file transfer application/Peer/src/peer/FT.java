/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package peer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import objectsender.Request;

/**
 *
 * @author malikali
 */
public class FT extends Thread{

    Request res ;
    String fileAddress;
    public static int BLOCK_SIZE = 4096;


    FT(Request r , String fileAdd)
    {
        res = r;
        fileAddress = fileAdd;
    }

    private void transferData(InputStream in, OutputStream out)
			throws IOException
	{
		byte b[] = new byte[BLOCK_SIZE];
		int amount;

		while ((amount = in.read(b)) > 0)
		{
			out.write(b, 0, amount);
		}
	}

    @Override
    public void run()
    {
        try {

            //connect to the peer who has requested the file....
            Socket s = new Socket(res.IP, res.portNUMBER);
            
            // store my ip address 
            res.IP = Main.myIP;

            // and port no of data socket
            ServerSocket ss = new ServerSocket(0);
            res.portNUMBER = ss.getLocalPort();
           
            // and set the file found variable.....
            res.isFound = true;

            // write the object on socket....
            ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
            oos.writeObject(res);

            oos.flush();
            //Close object Stream and Socket
            oos.close();
            s.close();

            //Wait for Request from Peer
            Socket ftsock = ss.accept();
            //ObjectInputStream ois = new ObjectInputStream (ftsock.getInputStream());
            // Now i need to streams.. one that will read from file - FileInputstream...
            // Other outputstream of socket...
            OutputStream os = ftsock.getOutputStream();
            File f = new File(fileAddress);
            FileInputStream fs = new FileInputStream(f);

            transferData(fs, os);

            ftsock.close();
            fs.close();
            os.close();

           // Main.print(ois.readObject());
        }
         catch (UnknownHostException ex) {
            Logger.getLogger(FT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FT.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
