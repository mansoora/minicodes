#include<iostream>
using namespace std;
class poly{
      private :
              int *cofi;
              unsigned int size;
              void del()
              {
                   if (cofi)
                   delete [] cofi;
              }
      public :
      poly():cofi(0),size(0){};
      poly( int s)//take size as input and initialize the polynomial with 0
      {
            size= s;
            cofi = new int [s];
            for (int i = 0 ; i < size ; i++)
            {
                *(cofi+i)=0;
            }   
      }
      poly(const poly & rhs)
      {
                 size = rhs.size;
                 cofi = new int [size];
                 for(int i = 0 ; i < size ; i++)
                         cofi[i] = rhs.cofi[i];
      }
      ~poly()
      {
             del();
      }
      bool  operator == (const poly & rhs)
      {
           if (this->size != rhs.size)
              return false;
           else
               for(int i = 0 ; i < size ; i++)
               if (this->cofi[i] != rhs.cofi[i])
                  return false;
               return true;
      }
      poly & operator=( const poly & rhs)
      {
           if (*this == rhs)
           return  *this ;
           
           this->del();
           this->size=rhs.size;
           cofi= new int [this->size];
           for (int i = 0 ; i < this->size; i++)
               this->cofi[i]=rhs.cofi[i];
           return *this;
      }
      poly & operator+( const poly & rhs)
      {
           poly *temp;
           if ( this->size>rhs.size )
           {
                 
                
                 temp = new poly(*this);
                 temp->cofi = new int [rhs.size];
                 for (int i = 0 ; i <this->size ;i++)
                     if (i<rhs.size)
                     temp->cofi[i] = cofi[i]+rhs.cofi[i];
                     else
                         temp->cofi[i]=this->cofi[i];
                     
                 return *temp;
           }
           else
           if (this->size<rhs.size )
           {
                 
                 temp = new poly(rhs);
                 for (int i = 0 ; i <rhs.size;i++)
                    if (i<size)
                     temp->cofi[i] = cofi[i]+rhs.cofi[i];
                     else
                         temp->cofi[i]=rhs.cofi[i];
                 return *temp;
           }
           else
           {     
                 temp = new poly;
                 for (int i = 0 ; i <size;i++)
                     temp->cofi[i] = cofi[i]+rhs.cofi[i];
                 return *temp;
           }
      }   
      poly & operator-( const poly & rhs)
      {
           if ( size>rhs.size )
           {
                 poly *temp;
                
                 temp = new poly(*this);
                 temp->cofi = new int [rhs.size];
                 for (int i = 0 ; i <size ;i++)
                     if (i<rhs.size)
                     temp->cofi[i] = cofi[i]-rhs.cofi[i];
                     else
                         temp->cofi[i]=cofi[i];
                     
                 return *temp;
           }
           else
           if (size<rhs.size )
           {
                 poly *temp;
                 temp = new poly(rhs);
                 for (int i = 0 ; i <rhs.size;i++)
                    if (i<size)
                     temp->cofi[i] = cofi[i]-rhs.cofi[i];
                     else
                         temp->cofi[i]=(-rhs.cofi[i]);
                 return *temp;
           }else
           {      
                  poly *temp;
                  temp = new poly(rhs);
                  for (int i = 0 ; i <size;i++)
                     temp->cofi[i] = cofi[i]-rhs.cofi[i];
                  return *temp;
           }
      }   
      poly & operator*(const poly & rhs)
      {
           int s = size+rhs.size-1; // -1 becoz we are taking +1 in size and both of them have +1 so v have to minus 1 
           poly *temp=new poly(s);
               for (int i = 0 ; i < size;i++)
                   for (int j = 0 ; j < rhs.size ; j++)
                       temp->cofi[i+j]+=rhs.cofi[j]* this->cofi[i];
           return *temp;     
      }
      poly & operator+=( const poly & rhs)
      {
           poly *temp;
           temp = new poly(*this);
                *temp = *temp + rhs;
           return *temp;
      }
      poly & operator-=( const poly & rhs)
      {
           poly *temp;
           temp = new poly(*this);
                *temp = *temp - rhs;
           return *temp;
      }
      poly & operator*=( const poly & rhs)
      {
           poly *temp;
           temp = new poly(*this);
                *temp = *temp * rhs;
           return *temp;
      }
      friend ostream & operator<<(ostream & , const poly & rhs)
      {
             cout<<"\n"<<endl;
           if(!rhs.cofi)
           {
                    cout<<"Polynomial is empty \n";
           }
           else
           {
           for (int i = rhs.size-1 ;i > 0 ; i--)
           {
               cout<<rhs.cofi[i]<<"X^"<<i;
               if (rhs.cofi[i-1]>=0)
               cout<<" + ";
           }
           cout<<rhs.cofi[0]<<"X^0";
           cout<<endl;
           } 
      }
      friend istream & operator>>(istream &, poly & rhs)       
     {
           if(rhs.size)
           rhs.del();
            int size=0;
           cout<<"Enter maximum Power of Polynomial"<<endl;
          
           cin>>size;
           
           while (size<0)
                 {
                      cout<<"Enter Positive Number "<<endl;
                      cin>>size;
                 }
           rhs.size=size+1;
           rhs.cofi = new int[rhs.size];
           for (int i = 0 ; i < rhs.size ; i++)
           {
               
               cout<<"Enter Value For X of Power "<<i<<endl;
               cin>>rhs.cofi[i];
           }
     }
};
int main()
{
    poly obj , obj2 , obj3;
    cin>>obj;
    cout<<obj<<endl;
    cout<<"object 2"<<endl;
    obj3 = obj+obj2;
    cout<<"After Adition"<<endl;
    obj3 = obj-obj2;
    cout<<"After Subtraction"<<endl;
    obj3= obj*obj2;
    cout<<"After Multiplication"<<endl;
    cout<<"After +="<<endl;
    obj+=obj2;
    cout<<"After -="<<endl;
    obj-=obj2;
    cout<<"After *="<<endl;
    obj*=obj2;
   
    system("pause");
    return 0;
}
