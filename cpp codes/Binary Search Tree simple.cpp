#include <iostream>
#include <conio.h>

using namespace std;

template <class t> class tree;

template <class t>
class node {
      friend class tree<t>;  //list can access all the data of class node 
      public:
      t val;
      node<t>* left;
      node<t>* right;
}; //end class node

template <class t>
class tree {
      public:
      node<t>* head ;
      node<t>* strt;
      tree(){
               cout << "tree constructor called - creating empty tree" << endl;
               head = NULL;
               node<t>* strt = new node<t>;               
      }
      
      void tree<t>::insert(t in){                     //insert
           
           if (head == NULL) {              
		      head = new node<t>;		      
		      head->val=in;
		      head->right=NULL;
              head->left=NULL;
              strt=head;
	       }
	       else if ( head != NULL ) {               
              if ( (head->val < in)) {
                   if (head->left == NULL) {
                      node<t>* t1 = new node<t>;            
                      t1->val=in;
                      t1->left=t1->right=NULL;
                      head->left=t1;
                   }
                   else if (head->left != NULL) { 
                           head=head->left;
                           insert(in);
                   }     
              }
              else if ( head->val >= in ) {
                   if (head->right == NULL) {
                      node<t>* t1 = new node<t>;             
                      t1->val=in;
                      t1->left=t1->right=NULL;
                      head->right=t1;
                   }
                   else if (head->right != NULL) {
                           head=head->right;
                           insert(in);
                   }
              }
           }                              
       } //end insert 
       
             

       
       
       void tree<t>::print_pre(node<t>* tmp){                   
                         cout<<(tmp->val)<<" ";
                         if (tmp->left!=NULL){
                            print_pre(tmp->left);
                         }
                         if (tmp->right!=NULL){
                            print_pre(tmp->right);
                         }       
       } //pre order traversal end 
       
       void tree<t>::print_post(node<t>* tmp){
                         if (tmp->left!=NULL){
                            print_pre(tmp->left);
                         }
                         if (tmp->right!=NULL){
                            print_pre(tmp->right);
                         }
                         cout<<(tmp->val)<<" ";
       } // post order traversal end
       
       void tree<t>::print_in(node<t>* tmp){                       
                         if (tmp->left!=NULL){
                            print_pre(tmp->left);
                         }               
                         cout<<(tmp->val)<<" ";
                         if (tmp->right!=NULL){
                            print_pre(tmp->right);
                         }
       } //in order traversal end
       
       //end print
       
       void tree<t>::del(){
            t in;
            node<t>* t1 = new node<t>;
            node<t>* t2 = new node<t>;
            t1 = strt ;
            t2 = strt ;
            
            cout<<"enter a the value to be deleted"<<endl;
            cin>>in;
            
            while ((t1->next->val) != in || t1->val!=in) {
                   t1=t1->next;
            }
            t2=t1->next->next;
            t1->next=t2;                                   
       } // end del  
            
            
            
            
                           
                          
}; // end class tree     


int main(){    
     int in;//node<int>* tmp= new node<int>;     
     tree<int> tree1 ;
     for (int i = 0 ; i < 6 ; i++){
         cout<<"enter the value"<<endl;
		 cin>>in;
         tree1.insert(in);
     }     
     tree1.print_pre(tree1.strt);
     getch();
     return 0;     
}
        