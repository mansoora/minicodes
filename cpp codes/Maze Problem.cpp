//Maze Problem. W represents Wall


#include <iostream>
#include <conio.h>
using namespace std;

int CountCells(char B[7][5], int x, int y);
int cells=1;


int main()
{
  /*  char board[4][5] = {'b','b','b','w','b',
                        'b','w','w','b','b',
                        'b','w','b','b','w',
                        'b','w','b','w','b'};*/
                        
  char board[7][5] = {'b','b','b','b','b',
                      'b','w','w','w','b',
                      'b','w','b','w','b',
                      'b','b','b','b','b', 
                      'b','w','w','w','b',
                      'b','w','b','w','b',
                      'b','b','b','b','b'};
                        
    int NoOfAreas=0;
    
    cout << "\n\n\nNumber of cells >>";
    
    for (int i=0; i<7; i++)
    {
        for (int j=0; j<5; j++)
        {
            if (board[i][j] == 'w')
            {
               NoOfAreas++;
               cells = 1;
               CountCells(board, i, j);
               cout << "\t" << cells ;
            }
        }
    }
    
    cout << "\n\n\nTotal number of areas are  " << NoOfAreas << "\n\n\n\n\n";
    
    system("pause");
    return 0;
}

int CountCells(char B[7][5], int x, int y)
{
    B[x][y] = '*';
    
    bool right=false, down=false;
    
    if (B[x+1][y]=='w' && x<7)
    {
       down = true;
       ++cells;
       CountCells(B, x+1, y);
    }
       
    if (B[x][y+1]=='w' && y<5)
    {
       right = true;
       ++cells;
       CountCells(B, x, y+1);
    }
    
    if (down==false && right==false)
       return 1;
}
