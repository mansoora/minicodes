#include<iostream>
using namespace std;




void selectionSort(int *array, int startIndex,int length)
{
    
    if ( startIndex >=length - 1 )
        return;
    int minIndex = startIndex;
    for ( int index = startIndex + 1; index < length; index++ )
    {
        if (array[index] < array[minIndex] )
            minIndex = index;
    }
    int temp = array[startIndex];
    array[startIndex] = array[minIndex];
    array[minIndex] = temp;
    selectionSort(array, startIndex + 1 , length);
}


int main()
{
    int a[9]={19,4,10,11,23,1,9,2,67};
   
    selectionSort(a,0,9);
    
    for (int i=0; i<9; i++)
        cout<<a[i]<<endl;
    
    cout<<endl;
    system("pause");
    return(0);
}
