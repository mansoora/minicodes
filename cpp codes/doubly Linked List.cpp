#include<iostream>
using namespace std;
template <class T>
class node{
      public:
           T data;
           node *next;
           node *prev;
           
           node(): data(0),next(0),prev(0){};
           node(T d):data(d),next(0),prev(0){};
           node(T d , const node *n , const node *p)
           {
                  data = d; next = n ; prev = p;
           }
};
template <class T>
class dlist{
           private:
                 node<T>  *head;
                 node<T>  *tail;
                 int size;
                void deletelist()
                {
                     if (head)
                     {
                      node<T> temp;
                      while ( head )
                            {
                             temp = head;
                             head=head->next;
                             head->prev=0;
                             temp->next=0;
                             delete temp;
                            }
                      head=0;
                      tail=0;
                      }
                }
           public:
                  dlist():head(0),tail(0), size(0){};
                /*  dlist ( const dlist<T> &rhs)
                  {
                        if ( rhs.head )
                        {
                            node<T> *temp = rhs.head;
                            node<T> nnode ;
                            nnode = new node <T>;
                            nnode->data = rhs.head->data;
                            head = nnode;
                            while( temp)
                            {
                                   temp = temp->next;
                                   insertatlast(temp->data);  
                            }
                        }
                        size = rhs.size;
                  }*/
                  void insertatlast(T d)
                  {
                       if (head)
                       {  
                       
                          node<T> *temp;
                          temp = new node<T>;
                          temp->data = d;
                          tail->next = temp;
                          
                          temp->prev = tail;
                          temp->next =0;
                          tail=tail->next;
                       }
                       else
                       {
                           head = new node<T>;
                           head->data = d;
                           tail = head;
                           head->next = NULL;
                           head->prev = NULL;
                       }                       
                       size++;
                  }
                  void insertatfront(T d)
                  {
                       if (!head)
                       {
                           head = new node<T>;
                           head->data = d;
                           tail = head;
                           head->next = NULL;
                           head->prev = NULL;
                       }
                       else
                       {
                           node<T> *temp;
                           temp = new node<T>;
                           temp->data = d;
                           temp->next = head;
                           head->prev = temp;
                           temp ->prev = NULL;
                           head = temp;
                       }
                       size++;
                  }
           void print()
           {
                node<T> *q;
                q = head;
                while(q)
                {cout<<q->data << "  ";
                q=q->next;}
                cout<<endl;
            }
            void add ( dlist<T> &rhs)
            {
                 node<T> *temp1 , *temp2;
                 if ( size > rhs.size )
                     for (int i = 0 ; i <= size - rhs.size ; i++)
                         rhs.insertatfront(0);
                 else
                     if ( size < rhs.size)
                     for (int i= 0 ; i <= rhs.size - size ; i++)
                         insertatfront(0);
                 temp1= tail;
                 temp2=rhs.tail;
                 while (temp1 != head)
                 {
                       int sum = 0 ; int carry = 0;
                       sum = temp1->data+temp2->data;
                       if ( sum>9 )
                       {
                            sum-=10;
                            carry++;     
                       }
                       temp1->data = sum;
                       temp1->prev->data+=carry;
                       temp1=temp1->prev;
                       temp2=temp2->prev;
                 }
                  int sum = 0 ; int carry = 0;
                  sum = temp1->data+temp2->data;
                  if ( sum>9 )
                  {
                     sum-=10;
                     insertatfront(1);
                  }
                  temp1->data=sum;
            }
            void sub (dlist<T> &rhs)
            {
                 node<T> *temp1 , *temp2;
                 if ( size > rhs.size )
                     for (int i = 0 ; i <= size - rhs.size ; i++)
                         rhs.insertatfront(0);
                 else
                     if ( size < rhs.size)
                     for (int i= 0 ; i <= rhs.size - size ; i++)
                         insertatfront(0);
                 temp1= tail;
                 temp2=rhs.tail;
                 while (temp1 != head)
                 {
                       
                       if ( temp1->data < temp2->data)
                       {
                            temp1->data+=10;
                            temp1->prev->data--;
                       }
                       
                       temp1->data -= temp2->data;
                       temp1=temp1->prev;
                       temp2=temp2->prev;
                 }
                 
            }
};

int main()
{
    dlist <int> d1;
    d1.insertatfront(9);
    d1.insertatlast(6);
    d1.insertatlast(1);
    d1.insertatfront(8);
    d1.print();
    dlist <int> d2;
    d2.insertatfront(5);
    d2.insertatfront (3);
    d2.insertatlast(9);
    d2.insertatfront(5);
    d2.print();
    d1.add(d2);
    d1.print();
    d1.sub(d2);
    d1.print();
   // d2.print();
    system("pause");
    return 0;
}
