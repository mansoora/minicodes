/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Malik Ali
 */
public class BomberMan {
        ArrayList<Image> up;
        ArrayList<Image> down;
        ArrayList<Image> left;
        ArrayList<Image> right;
        public  int positionx;
        public  int positiony;
        private int height;
        private int width;
        private boolean Killed = false; 
        private int lives; 
        private final int BufferValue = 1;
       
        public BomberMan(int l, int x , int y )
        {
            lives = l;
            positionx = 0;
            positiony = 0;
            up = new ArrayList<>();
            down = new ArrayList<>();
            left = new ArrayList<>();
            right = new ArrayList<>();
            initialise();
            
            
        }
        public void initialise()
        {
            for(int i = 1 ; i <= 8 ; i++)
            {
                
                
                up.add( MainClass.loadImage("sprites/player/up/"+i+".png"));
                down.add( MainClass.loadImage("sprites/player/down/"+i+".png"));
                left.add( MainClass.loadImage("sprites/player/left/"+i+".png"));
                right.add( MainClass.loadImage("sprites/player/right/"+i+".png"));
                
                /*
                up.add(  MainClass.loadImage("sprites/player/down/1.png"));
                down.add( MainClass.loadImage("sprites/player/down/1.png"));
                left.add( MainClass.loadImage("sprites/player/down/1.png"));
                right.add( MainClass.loadImage("sprites/player/down/1.png"));
                */
            }
            height = up.get(0).getHeight(null);
            width = up.get(0).getWidth(null);
                    
            
        }
        public Image getup(int ind)
        {
            return up.get(ind);
        }
        public Image getdown(int ind)
        {
            
            return down.get(ind);
        }
        public Image getleft(int ind)
        {
            return left.get(ind);
        }
        public Image getright(int ind)
        {
                return right.get(ind);
        }
        
        public int getbufferValue()
        {
            return BufferValue;
        }
        public int getWidth()
        {
            return width;
        }
        public int getheight()
        {
            return height;
        }
        public void killBomberMan()
        {
            lives--;
            Killed = true;
        }
        public boolean isKilled()
        {
            return Killed;
        }
        public int getLives()
        {
            return lives;
        }
}
