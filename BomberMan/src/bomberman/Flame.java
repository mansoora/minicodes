/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Malik Ali
 */
public class Flame {
    
    public  int positionx;
    public  int positiony;
    private int height;
    private int width;
    private int flameIndex = 0;
    private int flameTimer = 10;
    private int BufferValue = 4 ;
    ArrayList<Image> flameImage = new ArrayList<>();
    public Flame(String Path , int count)
    {
        for(int i = 1 ; i <= count ; i++)
        {
            flameImage.add( MainClass.loadImage(Path+i+".png"));
        }
        height = flameImage.get(0).getHeight(null);
        width = flameImage.get(0).getWidth(null);
    }
    public int getHeight()
    {
        return height;
    }
    public int getWidth()
    {
        return width;
    }
    public Image getFlame()
    {
        flameTimer--;
        if(flameTimer <= 0)
        {
            flameIndex++;
            flameTimer = 10;
        }
        if(flameIndex >= flameImage.size())
        {
            return null;
        }
        
        return flameImage.get(flameIndex);
    }
    public int getBufferValue()
    {
        return BufferValue;
    }
}