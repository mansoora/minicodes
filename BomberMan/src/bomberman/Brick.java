/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;

/**
 *
 * @author Malik Ali
 */
public class Brick {
    private Image brickImage = MainClass.loadImage("sprites/brick.png");
    private  int positionx;
    private  int positiony;
    private int height;
    private int width;
    
    private final int BufferValue = 2;
   /* public Brick()
    {
        positionx = 0;
        positiony = 0;
        height = brickImage.getHeight(null);
        width =  brickImage.getWidth(null);
    }*/
    public Brick(int x , int y)
    {
        positionx = x;
        positiony = y;
        height = brickImage.getHeight(null);
        width =  brickImage.getWidth(null);
    }
    public Image getImage()
    {
        return brickImage;
    }
    public int getHeight()
    {
        return height;
    }
    public int getWidth()
    {
        return width;
    }
    public int getBufferValue()
    {
        return BufferValue;
    }
    public int getx()
    {
        return  positionx;
    }
    public int gety()
    {
        return positiony;
    }
}
