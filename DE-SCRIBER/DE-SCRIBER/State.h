#pragma once
#include "WordPad.h"
#include "Encryption.h"
//#include "SpellCheck.h"
#include <fstream>

using namespace System;
using namespace System::Windows::Forms;

namespace DESCRIBER{


class State
{

#pragma region Class Members
private:
	WordPad _MyDocument;
public:
	char _Doc_ADDR[255];
	bool isSaved;
#pragma endregion


public:

	State(void)
	{
		for(Byte i=0; i<255; i++)
			_Doc_ADDR[i] = '\0';
		isSaved = false;
	}

	void StartUp(RichTextBox^ MyBox)
	{
		#pragma region Load From Temp
			ifstream in,in1,in2;
			in.open("C:/DESCRIBER/Temp-3.txt"); //for basic Info!
			if (in)
			{
				unsigned int Chunks;
				in>>Chunks;//lets say 3
				unsigned int* Line = new unsigned int [Chunks];
				for (unsigned int i=0; i<Chunks; i++)
				{
					in>>Line[i];//let say 2
					_MyDocument.CurChunk(1);
					_MyDocument._MyChunk[0].CurLine(Line[i]);
				}
				unsigned int** Words = new unsigned int* [Chunks];
				for (unsigned int i=0; i<Chunks; i++)
					Words[i] = new unsigned int [Line[i]];
				for (unsigned int i=0; i<Chunks; i++)
				for (unsigned int j=0; j<Line[i]; j++)
					in>>Words[i][j];
				in.close();//Completed Reading Data Info

				//Reading words... nd trying FILE # 2 along
				in1.open("C:/DESCRIBER/Temp-1.txt"); //Line settled words
				in2.open("C:/DESCRIBER/Temp-2.txt"); //Word Properties
				bool sp = false;
				unsigned int col = 0;
				unsigned int size = 0;
				char* ch;
				for (unsigned int i=0; i<Chunks; i++)
					for (unsigned int j=0; j<Line[i]; j++)
						for (unsigned int k=0; k<Words[i][j]; k++)
						{
							ch = new char;
							in1>>ch;
							in2>>sp;
							this->AddWordToDLL(i,j,k+1,ch,sp,size,col);
						}
				//Now we have line numbers!! whats next?? aumm...
			  in.close();
			  in1.close();
			}
			#pragma endregion
		isSaved = true;
		//DLL2RTB(MyBox);
		if (strlen(_Doc_ADDR) != 0)
		{
		Encrpytion obj;
		obj.isEncryption = false;
		obj.encrypt("C:/DESCRIBER/Temp-1.txt");
		}
		System::IO::StreamReader^ sr = gcnew System::IO::StreamReader("C:/DESCRIBER/Temp-1.txt");
		MyBox->Text =  sr->ReadToEnd();
		sr->Close();
	}

	//Called By Controller::StartUp()
	void AddWordToDLL(int Chunk, int Line, int Word, char* ch, bool sp, int size, int col)
	{
		_MyDocument.AddWordToDLL(Chunk, Line, Word, ch, sp, size, col);
		//_MyVisual.AddWord(Chunk,Line,Word,ch);
	}

	//replace
	void FindNReplace(RichTextBox^ MyBox)
	{
	}

	//AUTO STORE
	void AutoStore(RichTextBox^ MyBox)
	{
		isSaved = true;
		RTB2DLL(MyBox);
		//write file 1 2 3
		ofstream of3;
		of3.open("C:/DESCRIBER/Temp-3.txt");
		of3<<_MyDocument.CurChunk()<<endl;
		of3<<_MyDocument._MyChunk[0].CurLine();
		for(Byte i=0; i<_MyDocument._MyChunk[0].CurLine(); i++)
			of3<<endl<<_MyDocument._MyChunk[0]._MyLine[i].Size();
		of3.close();
		unsigned int NoLines = _MyDocument._MyChunk[0].CurLine();
		ofstream of,of2;
		of.open("C:/DESCRIBER/Temp-1.txt");
		of2.open("C:/DESCRIBER/Temp-2.txt");
		DNode<char*>* Temp;
		for (Byte i = 0 ; i < NoLines ; i++)
		{
			Temp = _MyDocument._MyChunk[0]._MyLine[i].getHead();
		while (Temp!=NULL)
		{
		    char* MyWord = new char;
			MyWord = Temp->Data();
			if (Temp->Next()!=NULL)
				of<<MyWord<<" ";
			else
				of<<MyWord;
			of2<<Temp->Sp_Error()<<" "<<Temp->FontSize()<<" "<<Temp->Colour()<<endl;
			Temp = Temp->Next();
		}
		delete Temp;
		of<<endl;
		}
		of.close();
		of2.close();
	}

	void RTB2DLL(RichTextBox^ MyBox)
	{
		int Line = _MyDocument._MyChunk[0].CurLine();
		for(int i =0; i<Line; i++)
			_MyDocument._MyChunk[0]._MyLine[i].DeleteList();
		int Lines = MyBox->Lines->Length;
		_MyDocument.CurChunk(1);
		_MyDocument._MyChunk[0].CurLine(Lines);
		//mTree<char> obj2;
		for (int i=0; i<Lines; i++)
		{
			String^ Mystr ;
			Mystr = MyBox->Lines[i]->ToString();
			int len = Mystr->Length;
			char* ch = new char[len];
			int j;
			for(j=0; j<len; j++)
				*(ch+j) = Mystr[j];
			*(ch+j) = '\0';
			unsigned int WCount = 0;
			int k =0;
			char* Word = new char;
			for(j=0; j<=len; j++)
			{
				if ( (*(ch+j)!=' ') && (j!=len) )
				{
					*(Word+k) = *(ch+j);
					k++;
				}
				if ( (*(ch+j)==' ') || (j==len) )
				{
					WCount++;
					*(Word+k) = '\0';
					k = 0;
					bool sp =true;
					//sp = obj2.search(Word);
					unsigned int size = 0;
					
					for (Byte a=0; a<i; a++)
						size += MyBox->Lines[a]->Length;
					size += (j-strlen(Word));
					MyBox->Select(size+1,strlen(Word));
					size = MyBox->SelectionFont->Size;
					unsigned int col = ColorEnum(MyBox->SelectionColor);
					_MyDocument.AddWordToDLL(0, i, WCount, Word, sp, size, col);
					//MessageBox::Show(Convert::ToString(*(Word+0)));
				}
			}
		//delete Word;
		//delete ch;
		}
	}
	//Ctrl + S
	void SaveNow(RichTextBox^ MyBox)
	{
		AutoStore(MyBox);
		//write file 4
		Encrpytion obj;
		obj.MakeFinalFile(_Doc_ADDR);
	}
	//Enum
	unsigned int ColorEnum(System::Drawing::Color^ Col)
	{
		//MessageBox::Show(Col->ToString());
					if (Col ==System::Drawing::Color::Red)
						return 0;
					if(Col == System::Drawing::Color::Black)
						return 1;
					if(Col == System::Drawing::Color::Blue)
						return 2;
					if(Col == System::Drawing::Color::Green)
						return 3;
					if(Col == System::Drawing::Color::Yellow)
						return 4;
	}
	
	//Ctrl + . inc
	void FontSizeInc(RichTextBox^ MyBox)
	{
		float s = MyBox->SelectionFont->Size;
		if (s<95) s+=4.0;
		MyBox->SelectionFont = gcnew Drawing::Font(MyBox->Font->FontFamily->Name,(float)(s));
	}

	//Ctrl + , dec
	void FontSizeDec(RichTextBox^ MyBox)
	{
		float s = MyBox->SelectionFont->Size;
		if (s>4) s-=4.0;
		MyBox->SelectionFont = gcnew Drawing::Font(MyBox->Font->FontFamily->Name,(float)(s));
	}

	//find
	void Find(RichTextBox^ MyBox)
	{
	}

	//TAB
	void SwitchTAB(Form^ MyForm)
	{
	}

	//OPEN
	void OpenDoc(Form^ MyForm)
	{
		if ( strlen(_Doc_ADDR) ==0 )
		{
			OpenFileDialog^ obj1 = gcnew OpenFileDialog();
			obj1->Filter = "DE-SCRIBER Files (*.des)|*.des";
			if (obj1->ShowDialog() == DialogResult::OK)
			{
				MessageBox::Show(obj1->FileName);
				String^ mystr = obj1->FileName;
				char* ch = new char [mystr->Length];
				Byte i;
				for(i=0; i<mystr->Length; i++)
					*(ch+i) = mystr[i];
				*(ch+i) = '\0';
				Encrpytion obj;
				obj.MakeTempFile(ch);
			}
		}
		else
		{
			Encrpytion obj;
			obj.MakeTempFile(_Doc_ADDR);
		}
		this->StartUp( ((RichTextBox^) ((TabControl^)(MyForm->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]));
	}

	//NEW
	void NewDoc(Form^ MyForm)
	{
	}

	//SAve n Close
	void SavenClose(Form^ MyForm)
	{
	}

};

};