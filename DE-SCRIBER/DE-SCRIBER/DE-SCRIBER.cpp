// DE-SCRIBER.cpp : main project file.

#include "stdafx.h"
#include "Controller.h"
#include "Loader.h"
#include "MAINFORM.h"

using namespace DESCRIBER;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	Controller* obj = new Controller();
	//Windows::Forms::ApplicationContext^ obj1;
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	if ( (args->Length > 0) )
	{
	// Create the main window and run it
	char* mystr = new char[args[0]->Length];
	for (Byte i=0; i<args[0]->Length; i++)
		mystr[i] = args[0][i];
	mystr[args[0]->Length] = '\0';
	Application::Run(gcnew DESCRIBER::Loader(obj,mystr));
	}
	else
	Application::Run(gcnew DESCRIBER::Loader(obj));
	//run application for loader!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! loader.h
	Application::Run(gcnew DESCRIBER::MAINFORM(obj));
	return 0;
}
