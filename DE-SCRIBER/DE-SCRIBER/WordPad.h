#pragma once
#ifndef INCLUDED_DLL
#include "DLL.h"
#define INCLUDED_DLL true
#endif
#include "Chunk.h"


namespace DESCRIBER{

class WordPad
{

#pragma region Class Members
public:
	unsigned int _CurChunk;
	unsigned int MAX_Chunk;

#pragma endregion


public:
	Chunk* _MyChunk;

	WordPad(void)
	{
		_CurChunk = 0;
		MAX_Chunk = 10;
		_MyChunk = new Chunk [MAX_Chunk];
	}

	unsigned int CurChunk()
	{
		return _CurChunk;
	}
	void CurChunk(unsigned int p)
	{
		_CurChunk = p;
	}
	void AddWordToDLL(unsigned int Chunk, unsigned int Line, unsigned int Word, char* ch, bool sp, unsigned int size, unsigned int col)
	{
		if ( Chunk < MAX_Chunk )
			_MyChunk[Chunk].AddWordToDLL(Line,Word,ch,sp,size,col);
	}

	void AddCursor(char* ch, int key) //space = 0; enter=1 delete = 2
	{
		//AddToChunk--0
			//AddTo DLL

		//AddToChunk--1
			//AddTo DLL
		//AddChunk

		//Remove4rmChunk--2
			//Remove4rmDLL
	}

};
};