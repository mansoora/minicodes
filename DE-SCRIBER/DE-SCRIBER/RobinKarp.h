#include<iostream>
#include<fstream>
using namespace std;




class RobinKarp{
private:
int power (int n , int p)
{
    if (p==0)
       return 1;
    else
        if (p==1)
        return n ;
    else
        return n*power(n,p-1);
}

int hash (char *s , int ini , int len)
{
    int h = 0 ; 
    
    for (int i = ini ; i<ini+len ; i++)
    {
        int ascii = s[i];
        if (ascii >=97 &&  ascii<= 122)
                  ascii-=32;
        h+= power(32 ,ini+len -i)*(ascii) ;
    }
   return h;
}

void extractwords(char *c , char **d )
{
     int w = 0 ;
      int pre = 0 ;
    int k = 0;
    int i = 0 ;
    while (c[i]!= '\0')
    {
          if (i == strlen(c)-1 && i - pre >= 3)
          {
                if (c[pre]== ' ')
                       pre++;
                    for (Byte q = pre ; q < strlen(c) ; q++)
                    {
                        d[w][q-pre] = c[q]; 
                    }
                    d[w][strlen(c)-pre]='\0';
          }
          if (c[i]== ' ')
          {
              if (i - pre > 3 )       
              {
                    if (c[pre]== ' ')
                       pre++;
                    for (int q = 0 ; q < i-pre ; q++)
                    {
                        d[w][q] = c[pre+q]; 
                    }
                    d[w][i-pre]='\0';
                    w++;
              }
              pre = i;       
          }
          
          i++;
    }
}

int checkwords(char *c )
{
    int pre = 0 ;
    int k = 0;
    int i = 0 ;
    while (c[i]!= '\0')
    {
          if (i == strlen(c)-1 && i - pre >= 3)
             k++;
          if (c[i]== ' ')
          {
              if (i - pre > 3 )       
                 k++;
              pre = i;       
          }
          i++;
    }
    
    return k;
}

int karp(char  **s1 , char* sub , int nol)
{
    int l = strlen (sub) , count = 0;
    int h1 = hash(sub , 0 , l);
    for (int j = 0 ; j < nol ; j++)
    for (Byte i = 0 ; i  < strlen(s1[j]) ; i++)
    {
        int h2 = hash (s1[j] ,i, l);
        if (h1 == h2 )
        {
           count++;
        }
    }
    return count;
}

int countword(char **c , int nol )
{
    int w = nol ;
    for ( int j = 0 ; j < nol ; j++)
    for (Byte i = 1 ; i < strlen(c[j])-1 ; i++)
    {
        if ( (c[j][i] == ' ' && c[j][i-1] != ' ')||( c[j][i] == '\n' && c[j][i-1] !=' ' ) )
        {
             w++;
        }
         
    }
    return w;
}
public:
RobinKarp(void)
{
}
static char* fnr(char  *source , char* find,char *replace ,bool* ref)
{
    if(strlen(source)<strlen(find))
    {
         ref = false;
         return 0;
    }
    int l = strlen(find) , count = 0;
    char *temp ;
    temp = new char [strlen(source)-strlen(find)+strlen(replace)];
    for(Byte i = 0 ; i < strlen(source)-strlen(find)+strlen(replace) ; i++)
    {
            *(temp+i) = ' ';
    }
    int h1 = hash( find , 0 , l);
    int dif = 0;
    for (Byte i = 0 ; i  < strlen(source) ; i++)
    {
        *(temp+i+dif) = *(source+i);
        int h2 = hash ( source ,i, l );
        if (h1 == h2 )
        {
           *ref = true;
           for(Byte j = i ; j <i+strlen(replace) ; j++)
          {
              *(temp+j)=*(replace+j-i);
          }
          i+=strlen(find)-1;
          dif=strlen(replace)-strlen(find);
        }
    }
    *(temp+strlen(source)-strlen(find)+strlen(replace))='\0';
    return temp;
}

void doitonfile(ifstream &in)
{
        char **c;
     if (!in)
    {
            cout<<"file not found"<<endl;
            system("pause");
            exit(1);
    }
    
    int v = 0 ; 
    c=new char* [45];
    while(!in.eof())
    {           
         c[v]= new char [255];       
         in.getline(c[v], 255,'\n');
         v++;
    }
     int totalw = countword(c , v);
     
     char *z = new char ;
     cin.get(z , 255 , '\n');
     int wc = checkwords(z);
     if (wc >0)
     {
            char **extracted ;
            extracted = new char *[wc];
            for(int i = 0 ; i  < wc ; i++)
                 extracted[i] = new char[100];
            extractwords(z , extracted);
            int matches = 0; 
            for (int i = 0 ; i < wc ; i++)
            {
                matches+=karp(c , extracted[i] , v);
            } 
            cout<<" matches "<<matches<<" totalwords " <<totalw<<endl;
            cout<<(float) (matches *100)/totalw<<"%"<<endl;
     }
}

};