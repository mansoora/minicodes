#include<iostream>
using namespace std;

template <class T>
class SinglyLinkedList;





template <class T>
class SNode{
      
      private:
              T Data;
              SNode<T> *Next;
              friend class SinglyLinkedList<T>;
      public:
             SNode():Data(0),Next(NULL){}
             SNode(const T& d){Data=d;Next=NULL;}
             SNode(const T& d, const SNode<T> *ptr){Data=d;Next=ptr;}
             //~SNode(){Data=0;Next=NULL;}
             T getData(){return Data;}
             SNode<T>* getNext()const{return Next;}
      };

template <class T>
class SinglyLinkedList{
      
      private:
              SNode<T> *Head;
              unsigned int Size;
      public:
             SNode<T>* getHead(){return Head;}
             SinglyLinkedList():Head(NULL),Size(0){}
             SinglyLinkedList(SinglyLinkedList & rhs);
             void InsertAtLast(const T& d);
             void InsertAtFront(const T& d);
             void RemoveAtLast();
             T RemoveAtFront();
             void DeleteList();
             unsigned int getSize(){return Size;}
             SinglyLinkedList<T>& operator = (SinglyLinkedList<T>& rhs);
             void Print();
			 unsigned int Search (unsigned int d)
             {
                   
                   if(!Head)
                   {
                            return 0;
                   }
                   SNode<T> *node;
                   node = Head ;
                   while (node->Next != NULL)
                   {
                         if (node->Data.KeyAscii == d)
                                        return node->Data.keycode;
                         node = node->Next;
                   }    
                   return 0;
             }
      };
      
template <class T>
SinglyLinkedList<T>& SinglyLinkedList<T>::operator = (SinglyLinkedList<T> & rhs)
{
    if (this->getSize()>0)
       this->DeleteList();
    if (rhs.getSize()!=0)
    {
         this->Size = rhs.getSize();
         SNode<T> *Temp = rhs.Head;
         SNode<T> *Temp2 = new SNode<T>(rhs.Head->getData());
         this->Head = Temp2;
         Temp = Temp->Next;
         while (Temp!=NULL)
         {
               Temp2->Next = new SNode<T> (Temp->getData());
               Temp2 = Temp2->Next;
               Temp = Temp->Next;
         }
         Temp2->Next = NULL;
         return (*this);
               
    }
}

      
template <class T>
void SinglyLinkedList<T>::DeleteList()
{
     if (Head!=NULL)
     {
        SNode<T> *Temp= Head;
        SNode<T> *Temp2;
        while (Temp!=NULL)
        {
              Temp2 = Temp;
              Temp = Temp->Next;
			  Temp2->Next = NULL;
              delete Temp2;        
        }
        Head = NULL;
     }
     Size = 0;
}


template <class T>
SinglyLinkedList<T>::SinglyLinkedList(SinglyLinkedList<T>& rhs):Head(NULL),Size(0)
{
    if (rhs.getSize()!=0)
    {
          Size = rhs.getSize();
          SNode<T> *Temp =rhs.Head;
          SNode<T> *TempO;
          TempO = new SNode<T> (rhs.Head->Data);
          Head = TempO;
          Temp = Temp->Next;
          while (Temp!=NULL)
          {
                TempO->Next = new SNode<T> (Temp->Data);
                Temp = Temp->Next;
                TempO = TempO->Next;
          }     
    }
}


template <class T>
void SinglyLinkedList<T>::RemoveAtLast()
{
     if (Head!=NULL)
     {
        SNode<T> *Temp = Head;
        
        while( (Temp->Next!=NULL) && (Temp->Next->Next!=NULL) )
             Temp = Temp->Next;
        if ( (Temp == Head) && (Size<2) )
        {
                 delete Temp;
                 Head = NULL;
        }
        else
        {
                 delete Temp->Next;
                 Temp->Next = NULL;
        }
     Size--;
     }
}


template <class T>
T SinglyLinkedList<T>::RemoveAtFront()
{
     T TData;
     if (Head!=NULL)
     {
        SNode<T> *Temp = Head->Next;
        Head->Next = NULL;
        TData = Head->Data;
        delete Head;
        Head = Temp;
        Size--;
     }
     return TData;
}
      
template <class T>
void SinglyLinkedList<T>::InsertAtLast(const T& d)
{
     SNode<T> *Temp=Head;
     
     if (Head==NULL)
     {
        Temp = new SNode<T>(d);
        Head = Temp;
     }
     else
     {
         while (Temp->Next!=NULL)
           Temp=Temp->Next;
         Temp->Next = new SNode<T>(d);
     }
     Size++;
}


template <class T>
void SinglyLinkedList<T>::InsertAtFront(const T& d)
{
     SNode<T> *Temp = new SNode<T>(d);
     //assert(Temp!=0);
     
     Temp->Next = Head;
     Head = Temp;
     Size++;
}

template<class T>
void SinglyLinkedList<T>::Print()
{
     SNode<T> *Temp=Head;
     cout<<endl<<"Elements Of Singly Linked List"<<endl;
     
     while (Temp!=NULL)
     {
           cout<<Temp->getData()<<endl;
           Temp = Temp->Next;
     }
}
