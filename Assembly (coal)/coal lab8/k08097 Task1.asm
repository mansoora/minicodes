TITLE 

; Mansoor Ali Malik
; Coal Lab 8 Task1
;  k 080097

INCLUDE Irvine32.inc

.data
text byte  'Assembly lab 8',0

.code
main PROC

mov ecx , 20
p:

 mov eax , 9

 call Randomrange
 cmp eax ,3
 je b
 ja g
 
mov eax ,  1111b
jmp print

b:
 mov eax , 0001b
jmp print 

g:

mov eax , 0010b

print :
    call settextcolor
    mov edx , offset text
    call writestring
    call Crlf

loop 	p
	;call DumpRegs	; display the registers
	exit
main ENDP
END main