TITLE 

; Write your comments here:
; 
; 

INCLUDE Irvine32.inc

.data
lengthofinput byte 0
count = 9
hello byte 'hello dear',0
good byte 'good to see u there ',0
mystr byte count dup(" ")
.code


ReadMyStr PROC ; procedure to read string 


    mov ECX,count
    mov ESI,OFFSET mystr
    
    input:	
    call ReadChar
    cmp EAX, 13
    JE skip
    call Writechar
    mov [ESI],EAX
    inc ESI
    loop input

   skip:
   mov EBX,count   ; Count is the maximum length of the character
   sub EBX,ECX     ; Ecx is the (count - legth) od string 
   mov lengthofinput , bl; Ebx is the length of the string
 mov EAX,10 ; for next line
   call WriteChar

ret
ReadMyStr ENDP


main PROC
;code comes here 
call clrscr
call ReadMyStr
mov edx , offset hello
call writestring
call crlf
mov edx , offset good
call writestring

     
;code ends here
	;call DumpRegs	; display the registers
	exit
main ENDP

END main



