TITLE eWorm

INCLUDE Irvine16.inc

EWORM	STRUCT
	x BYTE ?   ; x-coord on screen
	y BYTE ?   ; y-coord on screen
	dir BYTE ? ; d-right, s-down, a-left, w-up
EWORM	ENDS

WALLS STRUCT
	x BYTE ?
	y BYTE ?
	filled BYTE ?
WALLS ENDS

.data

; splash

splash 	byte "                                                                             ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                         \ \        / /                                      ",0dh,0ah
		byte "                        __\ \  /\  / /__  _ __ _ __ ___                      ",0dh,0ah
		byte "                       / _ \ \/  \/ / _ \| `__| `_ ` _ \                     ",0dh,0ah
		byte "                      |  __/\  /\  / (_) | |  | | | | | |                    ",0dh,0ah
		byte "                       \___| \/  \/ \___/|_|  |_| |_| |_|                    ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "							(       Mansoor Ali         )					   ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                          ( press any key to continue )                      ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                               :: HOW TO PLAY ::                             ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "                        navigate `theDude` by ASDW keys                      ",0dh,0ah
		byte "                                                                             ",0dh,0ah
		byte "               `a` - left - `s` - down - `d` - right - `w` - up              ",0


; Chamak-Patti

line1 byte " status: Alive!                 eWorm v0.1          Level:        Score:      ",0dh,0ah
line2 byte "________________________________________________________________________________",0dh,0ah
gameOverText byte " status: GameOver!",0dh,0ah
gameCompText byte " status: Game Completed!!",0dh,0dh
levelCompText byte " status: Level Competed!!",0dh,0ah
consoleTitle byte "eWorm [APLHA]",0dh,0ah
currentScore word 0
levelScore word 0
currLevel byte 1
scoreInc word 5 ; +5 on every food

; eWorm - variables

MAXLEN = 100				; maximum length of the worm
STARTLEN = 6				; starting length of worm
mySnake EWORM MAXLEN DUP(<0,0,0>)	; structure array, initialized with all 0
currLength BYTE ?			; current length of the snake
inputs BYTE "asdw",0			; valid inputs

foodX BYTE ?
foodY BYTE ?

wormSym BYTE 177d;176d
foodSym BYTE 'O'

; Walls - variables

maxWalls = 80*25
lineSize = 80
lineSz byte 80
totalLevels word 9
levelName byte "levels.txt"
fileHandle word ?
buffer byte maxWalls dup(0)
readBytes word ?
fileWalls WALLS maxWalls dup(<0,0,0>)
wallSym BYTE 219d

; scoreThrshould
scoreThreshould word 20

.code

;---------------------------------
printString PROC USES SI
; prints a string
;---------------------------------
	mov ah, 40h
	mov bx, 1
	int 21h
	ret
printString ENDP

;---------------------------------
splashScreen PROC USES CX AX
;---------------------------------
	mov dx, offset splash
	call writeString
	call StopScreen
	ret
splashScreen ENDP

;---------------------------------
placeFood PROC USES AX DX
; places the food on foodX, foodY
;---------------------------------
	mov dh,foodY
	mov dl,foodX
	call Gotoxy
	mov al,foodSym
	mov ah,9
	mov bh,0
	mov bl,lightred
	mov cx,1
	int 10h
	ret
placeFood ENDP

;---------------------------------
incLength PROC USES AX SI DI
; increments the length of theDude
;---------------------------------
	call getHead
	mov si, di
	add si, TYPE EWORM
	
	mov ah, (EWORM PTR mySnake[di]).x
	mov al, (EWORM PTR mySnake[di]).y
	
	mov (EWORM PTR mySnake[si]).x,ah
	mov (EWORM PTR mySnake[si]).y,al
	
	inc currLength
	ret
incLength ENDP

;---------------------------------
foodEaten PROC USES AX
; eWorm has eaten the food?
; bl = 1 (true) 0 false
;---------------------------------
	call getHead
	mov bx, 0
	
	mov ah, (EWORM PTR mySnake[di]).x
	mov al, (EWORM PTR mySnake[di]).y
	
	cmp foodX, ah
	je fx
	jmp nf
fx:	cmp foodY, al
	je fy
	jmp nf
fy:	mov bl,1
nf:
	ret 
foodEaten ENDP

;---------------------------------
genFood PROC USES AX SI BX
; generates food! 
;---------------------------------
	; RandomRange - ax
	
sd:
	mov ax, 80
	call RandomRange
	mov foodX, al
	
	mov ax, 21
	call RandomRange
	add ax, 3
	
	mov foodY, al
	
	mov bh, foodX
	mov bl, foodY
	
	mov si, 0
	call wallCount ; returns wallCount in ax
	mov cx,ax

wl:	cmp bh, (WALLS PTR fileWalls[si]).y
	je bhe
	jmp len
bhe:cmp bl, (WALLS PTR fileWalls[si]).x
	je trl
	jmp len
trl:jmp sd
len:
	add si, TYPE WALLS
	loop wl
we:
	
	ret
genFood ENDP

;---------------------------------
getHead PROC USES CX
; places offset of mySnake[currLength]
; in di
;---------------------------------
	mov cx,0
	mov cl, currLength
	dec cl
	mov dx,0
	mov di,0
ls:	add di, TYPE EWORM
	loop ls
	ret
getHead ENDP

;---------------------------------
initWorm PROC USES DI BX CX AX 
; initializes the worm
;---------------------------------	
	mov di,0
	mov cx,0
	mov cl,currLength
	; initialize y with 3
	mov al, 0
	
lp:	mov (EWORM PTR mySnake[di]).x,al
	mov (EWORM PTR mySnake[di]).y,3
	mov (EWORM PTR mySnake[di]).dir,'d'
	add di, TYPE EWORM
	inc al
	loop lp
	ret
initWorm ENDP

;---------------------------------
validInput PROC USES DI CX
; checks if the input is valid
; returns bh = 1 (true) 0 (false)
;---------------------------------
	mov cx, 4
	mov di, offset inputs

lp:	cmp al,[di]
	je true
	inc di
	loop lp
	mov bh,0
	jmp en
true:	mov bh,1	
en:
	ret
validInput ENDP

;--------------------------------------------------
AdvanceCursor PROC
; Advances the cursor n columns to the right.
; taken from Irvine
;--------------------------------------------------
	pusha
L1:
	push cx
	mov  ah,3 
	mov  bh,0
	int  10h
	inc  dl 
	mov  ah,2
	int  10h
	pop  cx
	loop L1

	popa
	ret
AdvanceCursor ENDP

;---------------------------------
printWorm PROC USES DI CX
	LOCAL x : WORD, y : WORD
;---------------------------------
	push ax
	mov cl,currLength

	call getHead
	
	mov si,0
lp:	mov ah,(EWORM PTR mySnake[si]).x
	mov al,(EWORM PTR mySnake[si]).y

	push dx
	mov dh,al
	mov dl,ah
	call Gotoxy
	pop dx

	cmp ah,(EWORM PTR mySnake[di]).x
	je sx
	jmp els
sx:	cmp al,(EWORM PTR mySnake[di]).y
	je sy
	jmp els
sy:	
	push bx
	push cx
	mov al,wormSym
	mov ah,9
	mov bh,0
	mov bl,white
	mov cx,1
	int 10h
	mov cx,1
	call AdvanceCursor
	pop cx
	pop bx	
	jmp bqi
els:
	push bx
	push cx
	mov al,wormSym
	mov ah,9
	mov bh,0
	mov bl,lightgreen
	mov cx,1
	int 10h
	mov cx,1
	call AdvanceCursor
	pop cx
	pop bx
bqi:
	;call WriteChar	

	add si, TYPE EWORM
	loop lp
	
	mov di,0
	mov dh,(EWORM PTR mySnake[di]).y
	mov dl,(EWORM PTR mySnake[di]).x
	
	push ax
	call Gotoxy
	mov al,' '
	call WriteChar
	mov dh,0
	mov dl,74
	call Gotoxy
	mov ax, currentScore
	call writeInt
	pop ax
	
	pop ax	
	ret
printWorm ENDP

;---------------------------------
snakeSupport PROC USES AX CX DX SI
; mySnake[n] = mySnake[n+1]
;---------------------------------
	mov cx,0
	mov cl, currLength
	dec cl
	mov dx,0
	mov si,0
	mov di,0	

ls:	add si, TYPE EWORM
	mov ah,(EWORM PTR mySnake[si]).x
	mov al,(EWORM PTR mySnake[si]).y
	mov (EWORM PTR mySnake[di]).x,ah
	mov (EWORM PTR mySnake[di]).y,al
	add di, TYPE EWORM
	loop ls

	ret
snakeSupport ENDP

;---------------------------------
moveSnake PROC USES BX CX DX
; moves the snake around..
;---------------------------------
	call getHead

	cmp al, 'd'
	je right
	cmp al, 's'
	je down
	cmp al, 'a'
	je left
	cmp al, 'w'
	je up

	jmp en

right:	inc (EWORM PTR mySnake[di]).x
	jmp en

down:	inc (EWORM PTR mySnake[di]).y
	jmp en

left:	dec (EWORM PTR mySnake[di]).x
	jmp en

up:	dec (EWORM PTR mySnake[di]).y
	jmp en

en:	
	
	;-------------------------;
	; OUT OF BOUNDS DETECTION ;
	; x<0 , x>80 , y<0 , y>80 ;
	;-------------------------;
	
	cmp (EWORM PTR mySnake[di]).x,79
	jg xgr
	jmp c1
xgr:	mov (EWORM PTR mySnake[di]).x,0
c1:
	cmp (EWORM PTR mySnake[di]).x,0
	jl xlw
	jmp c2
xlw:	mov (EWORM PTR mySnake[di]).x,79
c2:
	cmp (EWORM PTR mySnake[di]).y,24
	jg ygr
	jmp c3
ygr:	mov (EWORM PTR mySnake[di]).y,2
c3:
	cmp (EWORM PTR mySnake[di]).y,2
	jl ylw
	jmp c4
ylw:	mov (EWORM PTR mySnake[di]).y,24
c4:

	ret
moveSnake ENDP


;---------------------------------
checkHit PROC USES CX SI DI BX
; checks if the worm is on itself
; returns al = 1 if hit, else 0
;---------------------------------
	call getHead
	mov ah, (EWORM PTR mySnake[di]).x
	mov al, (EWORM PTR mySnake[di]).y
	
	; looping through entire body
	mov cl, currLength
	dec cl
	dec cl
	
	mov di,0
	
lm:	cmp ah,(EWORM PTR mySnake[di]).x
	je tr
	jmp cn
	
tr:	cmp al,(EWORM PTR mySnake[di]).y
	je hit
	
cn:	add di, TYPE EWORM
	loop lm
	mov al,0
	jmp nd

hit:	mov al, 1
nd:
	; if already hit, no need to check with wall
	cmp al, 1
	je we
	; check wall hit
	call getHead
	mov bh, (EWORM PTR mySnake[di]).x
	mov bl, (EWORM PTR mySnake[di]).y
	
	mov si, 0
	push ax
	call wallCount
	mov cx,ax
	pop ax
wl:	cmp bh, (WALLS PTR fileWalls[si]).y
	je bhe
	jmp len
bhe:cmp bl, (WALLS PTR fileWalls[si]).x
	je trl
	jmp len
trl:mov al,1
	jmp we
len:
	add si, TYPE WALLS
	loop wl
we:
	ret
checkHit ENDP

;---------------------------------
getKey PROC USES BX
; recieves previous in al
; returns current key in al
;---------------------------------
	mov bx,0
	mov bl,al ; saving al in bl
	mov ax,0

	mov ah,6
	mov dl,0FFh
	int 21h
	jz do
	jmp dont
do:	mov al,bl
dont:
	call validInput
	cmp bh,1
	je en
	mov al,bl
en:
	; invalid directions check
	; if (bl == 'd' && al == 'a') al = 'd';
	; if (bl == 'a' && al == 'd') al = 'a';
	; if (bl == 's' && al == 'w') al = 's';
	; if (bl == 'w' && al == 's') al = 'w';
	cmp bl, 'd'
	je bld
	jmp s2
bld:	cmp al,'a'
	je ala
	jmp s2
ala:	mov al, 'd'

s2:
	cmp bl, 'a'
	je bla
	jmp s3
bla:	cmp al,'d'
	je ald
	jmp s3
ald:	mov al, 'a'

s3:
	cmp bl, 's'
	je bls
	jmp s4
bls:	cmp al,'w'
	je alw
	jmp s4
alw:	mov al, 's'

s4:
	cmp bl, 'w'
	je blw
	jmp s5
blw:	cmp al,'s'
	je als
	jmp s5
als:	mov al, 'w'

s5:

	ret
getKey ENDP

;--------------------------------
addWall PROC USES CX SI
; al = x ; ah = y
;--------------------------------
	; getting to the last empty wall in array
	mov si,0
	mov cx,maxWalls
ll:	cmp (WALLS PTR fileWalls[si]).filled,0
	je en
	add si, TYPE WALLS
	loop ll
en:
	mov (WALLS PTR fileWalls[si]).x,al
	mov (WALLS PTR fileWalls[si]).y,ah
	mov (WALLS PTR fileWalls[si]).filled,1
	
	ret
addWall ENDP

;--------------------------------
wallCount PROC USES SI
; ax = wallCount
;--------------------------------
	mov si,0
	mov ax,0
ll:	cmp (WALLS PTR fileWalls[si]).filled,0
	je en
	add si, TYPE WALLS
	inc ax
	jmp ll
en:
	cmp ax, 0
	je xx
	jmp nd
xx: ;mov ax,1
nd:
	ret
wallCount ENDP

;--------------------------------
initLevel PROC USES AX BX CX DX DI SI
;--------------------------------
	call clearLevel
	
	mov ax, 716Ch
	mov bx, 0
	mov cx, 1
	mov dx, 1
	mov si, offset levelName
	int 21h
	jc nk
	mov fileHandle, ax
nk:
	; reading file
	mov cx, 25
	mov bx, 0
	mov ah, 3Fh
	mov bx, fileHandle
	mov cx, maxWalls
	mov dx, OFFSET buffer
	int 21h
	jc en
	mov readBytes, ax

	; closing the file
	;mov ah, 3Eh
	;mov bx, fileHandle
	;int 21h
	;jc en
	
en:
	mov cx, maxWalls
	mov di, offset buffer
	mov ax, 0

lk:	push dx
	mov dl,[di]
	cmp dl,'1'
	pop dx
	je ed
	jmp nm
	
ed: push ax
	div lineSz
	call addWall
	pop ax
	
nm:	inc ax
	inc di
	loop lk

nd:
	ret
initLevel ENDP

;------------------------
printLevel PROC USES SI DX AX CX
;------------------------
	mov si,0
	call wallCount
	mov cx,ax
lp:	mov dh,(WALLS PTR fileWalls[si]).x
	mov dl,(WALLS PTR fileWalls[si]).y
	call Gotoxy
	
	; writing colored char
	push bx
	push cx
	mov al,wallSym
	mov ah,9
	mov bh,0
	mov bl,lightblue
	mov cx,1
	int 10h
	mov cx,1
	call AdvanceCursor
	pop cx
	pop bx	
	
	add si, TYPE WALLS
	loop lp

	ret
printLevel ENDP

;---------------------------------
clearLevel PROC USES CX SI AX
;---------------------------------
	mov si,0
	call wallCount
	mov cx,ax
	cmp cx,0
	je en
lx:	mov (WALLS PTR fileWalls[si]).filled,0
	add si, type WALLS
	loop lx
en:
	ret
clearLevel ENDP

;---------------------------------
gameOver PROC USES CX DX
; shows the gameOver screen
;---------------------------------
	mov dl,0
	mov dh,0
	call Gotoxy

	mov cx, sizeof gameOverText
	mov dx, offset gameOverText
	call printString	

	ret
gameOver ENDP

;---------------------------------
gameCompleted PROC USES CX DX
; shows the gameOver screen
;---------------------------------
	mov dl,0
	mov dh,0
	call Gotoxy

	mov cx, sizeof gameCompText
	mov dx, offset gameCompText
	call printString	

	ret
gameCompleted ENDP

;---------------------------------
showLevelComp PROC USES CX DX
; shows the gameOver screen
;---------------------------------
	mov dl,0
	mov dh,0
	call Gotoxy

	mov cx, sizeof levelCompText
	mov dx, offset levelCompText
	call printString	

	ret
showLevelComp ENDP

;---------------------------------
levelNameText PROC USES AX DX
; shows the levelname
;---------------------------------
	mov dl,59
	mov dh,0
	call Gotoxy

	mov al,currLevel
	call writeInt	
	
	ret
levelNameText ENDP

;---------------------------------
StopScreen PROC USES AX
;---------------------------------
	mov  ah,1
	int 21h
	ret
StopScreen ENDP 

main PROC
;code comes here 

	; initialization

	mov ax,@data
	mov ds,ax

	call splashScreen
	
	mov currLength,STARTLEN
	
	; main loop - for 'em levels!
	call Randomize
	mov cx,1
nw:	; clearing screen
	cmp cx, totalLevels
	je gameComp
	
	push cx ; main loop!

	call clrscr
	
	mov cx, sizeof line1
	mov dx, offset line1
	call printString

	mov cx, sizeof line2
	mov dx, offset line2
	call printString
	
	call levelNameText
	
	mov ax,0
	mov bx,0
	mov cx,0
	
	call initLevel
	call printLevel
	
	; initial direction
	
	mov al,'d' ; right
	call genFood
	call initWorm

	; game loop

lp:	call getKey
	call placeFood
	call printWorm
	call snakeSupport
	call moveSnake
	call foodEaten
	cmp bl, 1
	je et ; food eaten
	jmp nt

et:	call incLength
	call genFood
	push ax
	mov ax,scoreInc
	add levelScore,ax
	add currentScore, ax
	pop ax

nt: ; not eaten
	push ax
	
	mov ax,0
	call checkHit ; self hit and walls
	cmp al,1
	je en

	; check if score is reached

	mov ax,levelScore
	cmp ax,scoreThreshould
	je lcom ; level completed!

	; controlling gamespeed
	mov eax,400	
	call delay
	mov eax,0
	pop ax
	jmp lp ; jump for gameloop! // blind loop!

lcom: ; saman to do after level completion!
	pop ax
	mov levelScore,0
	inc currLevel
	call showLevelComp
	call StopScreen
	pop cx
	inc cx
	jmp nw
	
gameComp:
	call gameCompleted
	jmp nx
	
en:	call gameOver
nx:	call StopScreen
;code ends here
	exit
main ENDP
END main