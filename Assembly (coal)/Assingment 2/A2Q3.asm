TITLE 

; Assingment 2 Question Number 3
; Mansoor Ali	 
; K080097
; Objective : Successive Multiplication 

INCLUDE Irvine32.inc

.data
int1 WORD 0
int2 WORD 0
MULT WORD 0
mess  byte "Enter Integer " , 0
len = $-mess

error byte "multiplication by zero" , 0

lene =$- error 

.code

print proc
    mov ecx , len  ; storing length in ecx for loop
    mov esi , offset mess  ; stroing the offset(starting adress of mess)
    p:
    mov eax , [esi] ; stroring the value from the address of esi in eax
    call writechar  ; printing the eax value
    inc esi 	    ; incrementing esi to get next 	
    loop p	
ret
print ENDP

printe proc
    mov ecx , lene
    mov esi , offset error
    p:
    mov eax , [esi]
    call writechar
    inc esi
    loop p
ret
printe ENDP

checking2 proc
cmp int2 , 0  ; COMPARING SECOND NUMBER IF LESS THAN 0
jl endprogram
ret
checking2 endp 

checking1 proc
cmp int1 , 0
 jg retu    ; jump to return if greater than 0
 
call checking2 ; check for the second number     
retu:
ret
checking1 endp

multiply1 proc
   mov cx , int1
   mov ax , 0
   p:
   add ax , int2 ; SUCCESSIVLY ADDING THE NUMBER
   loop p
   MOV MULT  , AX  ; STORING AX IN VARIABLE
  ret 
multiply1 endp

multiply2 proc
   neg int1
   mov cx , int1
   mov ax , 0
   p:
   sub ax , int2
   loop p
   MOV MULT  , AX
   mov ax , MULT
   call writeint
    neg MULT  
  ret 
multiply2 endp

result proc
   mov ax , int1
   call writeint
   mov ax , '*'
   call writechar
   mov ax , int2
   call writeint
   mov ax , '='
   call writechar
   mov ax , mult
   call writeint
   mov ax , 32
   call writechar
   mov ax , int1
   call writeint
   mov ax , '+'
   call writechar
   mov ax , int2
   call writeint
   mov ax , '='
   call writechar
   mov bx , int1
   add bx , int2
   mov ax , bx
   call writeint	
  
ret
result endp

main PROC
call print ; call to print message 
MOV EAX , 0 
call readint

mov int1,ax ;moving eax in int1

call print
MOV EAX , 0 
call readint
mov int2,ax ;moving ax in int2
cmp int1 , 0
je endprog

cmp int2 , 0 ; IF NUMBER IS EQUAL TO ZERO THEN END
je endprog 

call checking1 ; checking for integers if both of them are negative or not

cmp int1 , 0
jl r

call multiply1
jmp q

r:
call multiply2

q:

call result
jmp endprogram

endprog:
call printe ; error message 

;	call DumpRegs	; display the registers
endprogram:: ; global label
	exit
main ENDP
END main