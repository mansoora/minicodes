TITLE 

; Assingment 2 Question Number 1
; Mansoor Ali	 
; K080097
; Objective : STORING PRE DE VALUES IN REGISTERS

INCLUDE Irvine32.inc

.data
     uarray dword 1000h , 2000h , 3000h , 4000h
     sarray  dword -1 , - 2 , -3 , -4 

.code
main PROC

      mov esi , offset uarray ; STORING INITIAL ADREES of uarray IN THE REGISTER
      mov eax , [esi] ; DEREFRENCING THE ADRESS AND STORIG VALUE IN ESI
      add esi , sizeof dword ; ADDING THE SIZE OF DWORD TO GET next value
      mov ebx , [esi]
      add esi , sizeof dword
      mov ecx , [esi]
      add esi , sizeof dword
      mov edx , [esi]
      call DumpRegs	

      mov esi , offset sarray ;STORING INITIAL ADREES of sarray IN THE REGISTER
      mov eax , [esi]
      add esi , sizeof dword
      mov ebx , [esi]
      add esi , sizeof dword
      mov ecx , [esi]
      add esi , sizeof dword
      mov edx , [esi]
		
call DumpRegs	; display the registers
	exit
main ENDP
END main