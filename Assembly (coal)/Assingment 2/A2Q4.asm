TITLE 
; Assingment 2 Question Number 4
; Mansoor Ali	 
; K080097
; Objective : to find all the divisers of a number 

INCLUDE Irvine32.inc

.data
num WORD ?

.code
diviser proc
    mov edx , 2
out1:
	mov ebx , 0 
        in1:
         add ebx , edx ; add ebx in edx 
	 cmp bx , num
	 jl in1   ; if ebx is less then number jump to in
         je print  ; IF EBX IS EQUAL TO THE INPUT THEN PRINT
    inc edx 
    cmp dx , num
    jl out1

jmp return ; unconditional jump to return

print: ; printing the value
mov eax , edx

call writeint

jmp in1 ; JUMP BACK


return:
ret
diviser endp


main PROC
p:

call readint

mov num , ax
cmp num , 0
jle p ;if num is less than 0 then take input again


CALL diviser ; CALLING PROCEDURE

call DumpRegs	; display the registers
	exit
main ENDP
END main