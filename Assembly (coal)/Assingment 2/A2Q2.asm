TITLE 
; Assingment 2 Question Number 2
; Mansoor Ali	 
; K080097
; Objective : check the Flags after mathematical operations
INCLUDE Irvine32.inc

.data
x DWORD 4
y DWORD 5
z DWORD 1
.code
main PROC
; Expresion : EAX = -4 +5 -1
call DumpRegs ; Zero Flag on Because We Dont have any value in eax
              ; Sign Falg is also Zero 
sub eax , x ; Zero Flag is Off bcoz ve have 2's complement of 4
 	    ; sign flag is on bcoz ve have negative value in eax 
call DumpRegs
add eax , y ; Zero Flag is Off becoz we have non zero value after arithmetic operation
	    ; SignFlag is off bcoz we have positive value in eax
call DumpRegs
sub eax , z ; Zero Flag is On bcoz we got Zero after arithmetic operation
	    ; Sign Flag is Off bcoz Zero has no sign
Call DumpRegs
	exit
main ENDP
END main