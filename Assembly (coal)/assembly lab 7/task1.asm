TITLE 

; Mansoor Ali
; K080097
; Lab 7 Task1

INCLUDE Irvine32.inc

.data
s byte "Conputer Organization and Assembly Langugae",0

.code
main PROC


mov ecx , 6 ; for six colours 

printing:
 mov eax , ecx ; colour numbers like 0 ,  1  , 2 
 add eax , (white *16)
 call settextcolor  ; to set the console colour
 mov edx , offset s ; moving offset to edx for printing string
 call writestring    
 mov eax , 10       ; for leaving the line
 call writechar
 loop printing


;code ends here
	exit
main ENDP
END main